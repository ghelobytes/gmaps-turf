var XXX;
(function($) {
    $(function() {
        var SELECTED_FEATURE = null;

        var gmap = new GMaps({
            div: '#map',
            lat: 14.55729793151005,
            lng: 121.02403874034121,
            click: unselectSelected
        });
        XXX = gmap.map;

        var drawingLayer = new google.maps.Data();
        drawingLayer.setStyle(function(feature) {
            return {
                editable: feature.getProperty('editable'),
                draggable: feature.getProperty('draggable')
            };
        })
        drawingLayer.setControls(['Point', 'LineString', 'Polygon']);

        drawingLayer.addListener('addfeature', updateGeoJSON);
        drawingLayer.addListener('removefeature', updateGeoJSON);
        drawingLayer.addListener('setgeometry', updateGeoJSON);

        drawingLayer.addListener('click', selectFeature);

        drawingLayer.setMap(gmap.map);


        function updateGeoJSON() {
            drawingLayer.toGeoJson(function(geojson) {
                console.log(geojson);
            });
        }

        function unselectSelected(){
            if(SELECTED_FEATURE) {
                SELECTED_FEATURE.setProperty('editable', false);
                SELECTED_FEATURE.setProperty('draggable', false);
            }
        }

        function selectFeature(event) {

            unselectSelected();

            SELECTED_FEATURE = event.feature;
            SELECTED_FEATURE.setProperty('editable', true);
            SELECTED_FEATURE.setProperty('draggable', true);

            SELECTED_FEATURE.toGeoJson(function(geojson) {
                $('#geojson').text(JSON.stringify(geojson));
            });

        }



    });
})(jQuery);
